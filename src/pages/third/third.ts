import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

@IonicPage()
@Component({
  selector: 'page-third',
  templateUrl: 'third.html',
})
export class ThirdPage {

	items: any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, public dataProvider: DataProvider) {

	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ThirdPage');
	}

	ionViewDidEnter() {

		console.log('creating interval');

		/*setInterval(() => {
	
			console.log('pushing lots of elements');

			for(let i=0; i < 100000; i++){
				this.items.push('uh oh');
			}

		}, 1000);*/

		let bigArray = [];

		for(let i=0; i < 1000; i++){
			bigArray.push(document.createElement('div'));
		}

		this.dataProvider.myData.push.apply(this.dataProvider.myData, bigArray);

		console.log("Array length: ", this.dataProvider.myData.length);

	}

}
