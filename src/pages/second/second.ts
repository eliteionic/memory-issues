import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-second',
  templateUrl: 'second.html',
})
export class SecondPage {

	items: string[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams) {

		this.items = ['just', 'a', 'few', 'elements', 'to', 'start', 'with'];

	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SecondPage');
	}

	doInfinite(infiniteScroll){
		
		for(let i=0; i < 100; i++){
			this.items.push('more items!');
		}

		infiniteScroll.complete();

	}

}
